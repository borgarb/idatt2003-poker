package edu.ntnu.idatt2003.borgarbarland.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <h1>PlayingHand</h1>
 * PlayingHand.
 *
 * <p>Class for representing a hand of cards</p>
 */
public class PlayingHand {

  private List<PlayingCard> cards = new ArrayList<>();

  /**
   * <h2>drawCardsFromDeck</h2>
   * drawCardsFromDeck.
   *
   * <p>Method for drawing cards from a deck</p>
   *
   * @param amount The amount of cards to draw
   * @param deck The deck to draw from
   */
  public void drawCardsFromDeck(int amount, DeckOfCards deck) {
    cards = deck.dealHand(amount);

    // Sort the cards
    cards.sort(Comparator.comparing(PlayingCard::getSuit).thenComparing(PlayingCard::getFace));
  }

  public int getSumOfFaces() {
    return getHand().stream().mapToInt(PlayingCard::getFace).sum();
  }

  /**
   * <h2>isFlush</h2>
   * isFlush.
   *
   * <p>Method for checking if the hand is a flush</p>
   *
   * @return True if the hand is a flush, false otherwise
   */
  public boolean isFlush() {
    // Stream the cards --> group by suit --> stream the groups --> check if any group has 5 or
    // more cards
    return getHand().stream().collect(Collectors.groupingBy(PlayingCard::getSuit))
        .values().stream().anyMatch(cardSuit -> cardSuit.size() >= 5);
  }

  /**
   * <h2>hasQueenOfSpades</h2>
   * hasQueenOfSpades.
   *
   * <p>Method for checking if the hand has the queen of spades</p>
   *
   * @return True if the hand has the queen of spades, false otherwise
   */
  public boolean hasQueenOfSpades() {
    // Stream the cards --> check if any card has the face 12 and the suit 'S'
    return getHand().stream().anyMatch(card -> card.getFace() == 12 && card.getSuit() == 'S');
  }

  /**
   * <h2>getHeartCards</h2>
   * getHeartCards.
   *
   * <p>Method for getting the heart cards in the hand</p>
   *
   * @return A list of the heart cards in the hand
   */
  public List<PlayingCard> getHeartCards() {
    // Stream the cards --> filter the cards that have the suit 'H' --> collect to list
    return cards.stream().filter(card -> card.getSuit() == 'H').toList();
  }

  public List<PlayingCard> getHand() {
    return cards;
  }
}
