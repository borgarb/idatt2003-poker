package edu.ntnu.idatt2003.borgarbarland.models;

import java.util.ArrayList;
import java.util.Collections;

/**
 * <h1>DeckOfCards</h1>
 * DeckOfCards.
 * 
 * <p>Class for representing a deck of cards</p>
 */
public class DeckOfCards {

  private final ArrayList<PlayingCard> cardStack = new ArrayList<>();
  private int maxDrawSize = 52;

  /*
   * <h2>Constructor</h2>
   * 
   * <p>Class representing a deck of cards</p>
   */
  public DeckOfCards() {
    restockCardStack();
  }

  public DeckOfCards(int maxDrawSize) {
    restockCardStack();
    this.maxDrawSize = maxDrawSize;
  }

  public void addCard(PlayingCard card) {
    cardStack.add(card);
  }

  public ArrayList<PlayingCard> getCardStack() {
    return cardStack;
  }

  /**
   * <h2>dealHand</h2>
   * dealHand.
   * 
   * <p>Method for picking out 'n' amount of cards from the deck</p>
   *
   * @param n the mount of cards to fetch
   * @return The list of drawn cards
   */
  public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {

    // Throw if player tries to draw a negative or an amount higher than the decks
    // card draw limit
    if (n > maxDrawSize || n < 1) {
      throw new IllegalArgumentException("You must draw between 1 and " + maxDrawSize + " cards!");
    }

    // Initialize variable for cards to be dealt
    ArrayList<PlayingCard> cardsToDeal = new ArrayList<>();

    // Draw cards and remove them from the stack
    for (int i = 0; i < n; i++) {
      cardsToDeal.add(cardStack.remove(cardStack.size() - 1));
    }

    // Return the drawn cards
    return cardsToDeal;
  }

  /**
   * <h2>restockCardStack</h2>
   * restockCardStack.
   *
   * <p>Restocks and reshuffles the entire deck of cards</p>
   */
  public void restockCardStack() {

    // Remove all cards left in the deck
    cardStack.clear();

    // Add all 52 cards
    char[] suits = { 'C', 'D', 'H', 'S' };
    for (int i = 1; i <= 13; i++) {
      for (char suit : suits) {
        addCard(new PlayingCard(suit, i));
      }
    }

    // Shuffle the cards
    Collections.shuffle(cardStack);
  }
}
