package edu.ntnu.idatt2003.borgarbarland;

import edu.ntnu.idatt2003.borgarbarland.ui.PokerHandPage;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * <h1>Main</h1>
 * Main.
 *
 * <p>Class for starting the application</p>
 */
public class Main extends Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    primaryStage.setScene(new Scene(new PokerHandPage(), 900, 450));
    primaryStage.show();
  }
}