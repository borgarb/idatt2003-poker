package edu.ntnu.idatt2003.borgarbarland.ui;

import javafx.scene.control.Alert;

/**
 * <h1>ErrorDialog</h1>
 * ErrorDialog.
 *
 * <p>Class for displaying error messages</p>
 */
public class ErrorDialog {
  /**
   * <h2>showErrorDialog</h2>
   * showErrorDialog.
   *
   * @param message The message to display
   */
  public static void showErrorDialog(String message) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("Error");
    alert.setContentText(message);
    alert.showAndWait();
  }
}
