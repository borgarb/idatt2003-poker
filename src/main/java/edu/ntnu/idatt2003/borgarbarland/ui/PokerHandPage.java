package edu.ntnu.idatt2003.borgarbarland.ui;

import edu.ntnu.idatt2003.borgarbarland.models.DeckOfCards;
import edu.ntnu.idatt2003.borgarbarland.models.PlayingCard;
import edu.ntnu.idatt2003.borgarbarland.models.PlayingHand;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * <h1>PokerHandPage</h1>
 * PokerHandPage.
 *
 * <p>Class for representing the poker hand page</p>
 */

public class PokerHandPage extends VBox {

  private final DeckOfCards deckOfCards = new DeckOfCards(15);
  private final PlayingHand hand = new PlayingHand();
  private final HBox cardDisplayArea = new HBox();
  private final HBox buttonArea = new HBox(20);
  private final Text cardCount = new Text();
  private final TextField handSize =  new TextField();
  private final Text handStateArea = new Text();

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>Creates a poker hand page</p>
   */
  public PokerHandPage() {
    super();

    // Set the spacing and padding
    setSpacing(40);
    setPadding(new javafx.geometry.Insets(40));

    // Initialize and set up the buttons
    initButtonArea();

    // Set up the hand size text field
    handSize.setMaxWidth(120);
    handSize.setAlignment(Pos.CENTER);
    handSize.setStyle("-fx-font-size: 20");
    handSize.setPromptText("Hand size");

    // Set up the hand state font size
    handStateArea.setStyle("-fx-font-size: 20");

    // Set up the card display area
    cardDisplayArea.setAlignment(Pos.CENTER);
    cardDisplayArea.setPadding(new javafx.geometry.Insets(10));

    // Set up the card count font size
    cardCount.setStyle("-fx-font-size: 20");

    // Put it all together and build the page
    setAlignment(Pos.CENTER);
    build();
    updateDisplay();
  }

  /**
   * <h2>build</h2>
   * build.
   *
   * <p>Method for building the poker hand page</p>
   */
  public void build() {
    getChildren().add(cardCount);
    getChildren().add(cardDisplayArea);
    getChildren().add(handStateArea);
    getChildren().add(buttonArea);
    getChildren().add(handSize);
  }

  /**
   * <h2>updateDisplay</h2>
   * updateDisplay.
   *
   * <p>Method for updating the display</p>
   */
  public void updateDisplay() {
    updateHandArea();
    updateCardCount();
    updateHandStateArea();
  }

  private void updateHandArea() {
    // Clear the display area
    cardDisplayArea.getChildren().clear();

    // If the hand has cards, display them
    if (!hand.getHand().isEmpty()) {

      // Get the cards and put them together in a string
      Text cards = new Text(hand.getHand().stream().map(PlayingCard::getAsString).reduce("",
          (allCardsSoFar, newCardString) -> allCardsSoFar + " " + newCardString).trim());

      // Set the font size
      cards.setStyle("-fx-font-size: 30");

      // Add the cards to the display area
      cardDisplayArea.getChildren().add(cards);

    // else display a message saying the hand is empty
    } else {
      Text noCardsMessage = new Text("No cards in hand");
      noCardsMessage.setStyle("-fx-font-size: 30");
      cardDisplayArea.getChildren().add(noCardsMessage);
    }
  }

  private void updateCardCount() {
    cardCount.setText("Cards left in deck: " + deckOfCards.getCardStack().size());
  }

  private void updateHandStateArea() {
    // if the hand is empty, display N/A
    if (hand.getHand().isEmpty()) {
      handStateArea.setText("Sum of faces: N/A | Is flush: N/A | Has queen of spades: N/A | "
          + "Hearts: N/A");
    // else display the hand state
    } else {
      // Get the heart cards and put them together in a string
      String heartCards = hand.getHeartCards().stream().map(PlayingCard::getAsString).reduce("",
          (cardsSoFar, newCard) -> cardsSoFar + " " + newCard).trim();

      if (heartCards.isEmpty()) {
        heartCards = "Null";
      }

      handStateArea.setText("Sum of faces: " + hand.getSumOfFaces() + " | Is flush: "
          + hand.isFlush() + " | Has queen of spades: " + hand.hasQueenOfSpades() + " | Hearts: "
          + heartCards);
    }
  }

  private void initButtonArea() {
    Button restockButton = new Button();
    restockButton.setPadding(new javafx.geometry.Insets(10));
    restockButton.setText("Restock the deck");
    restockButton.setOnAction(e -> {
      deckOfCards.restockCardStack();
      cardCount.setText(Integer.toString(deckOfCards.getCardStack().size()));
      updateDisplay();
    });

    Button drawCardsButton = new Button();
    drawCardsButton.setPadding(new javafx.geometry.Insets(10));
    drawCardsButton.setText("New hand");
    drawCardsButton.setOnAction(e -> {
      // Throw on illegal values
      try {
        if (handSize.getText().isEmpty()) {
          throw new IllegalArgumentException("Hand size cannot be empty");
        } else if (handSize.getText()
            .chars().anyMatch(character -> !Character.isDigit(character))) {
          throw new IllegalArgumentException("Hand size must be a positive integer!");
        } else if (Integer.parseInt(handSize.getText()) > deckOfCards.getCardStack().size()) {
          throw new IllegalArgumentException("Not enough cards left in the deck");
        }

        hand.drawCardsFromDeck(Integer.parseInt(handSize.getText()), deckOfCards);

      } catch (IllegalArgumentException exception) {
        ErrorDialog.showErrorDialog(exception.getMessage());
      }

      updateDisplay();
    });

    // Add the buttons
    buttonArea.getChildren().add(restockButton);
    buttonArea.getChildren().add(drawCardsButton);
    buttonArea.setAlignment(Pos.CENTER);
  }
}
