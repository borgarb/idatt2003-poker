package edu.ntnu.idatt2003.borgarbarland.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

  @Nested
  public class PositiveTests {

  private PlayingCard card1;
  private PlayingCard card2;

  @BeforeEach
  void setUp() {
    card1 = new PlayingCard('S', 12);
    card2 = new PlayingCard('S', 12);
  }

  @Test
  @DisplayName("Test equals")
  void testEquals() {
    assertTrue(card1.equals(card2));
  }

  @Test
  @DisplayName("Test getAsString")
  void testGetAsString() {
    String expected = "S12";
    String actual = card1.getAsString();

    assertEquals(expected, actual, "These should match!");
  }

  @Test
  @DisplayName("Test getFace")
  void testGetFace() {
    int expected = 12;
    int actual = card1.getFace();

    assertEquals(expected, actual, "These should match!");
  }

  @Test
  @DisplayName("Test getSuit")
  void testGetSuit() {
    char expected = 'S';
    char actual = card1.getSuit();

    assertEquals(expected, actual, "These should match!");
  }

  @Test
  @DisplayName("Test hashCode")
  void testHashCode() {
    assertEquals(card1.hashCode(), card2.hashCode(), "These should match!");
  }
 }

 @Nested
 public class NegativeTests {

  private PlayingCard card1;
  private PlayingCard card2;

  @BeforeEach
  void setUp() {
    card1 = new PlayingCard('S', 12);
    card2 = new PlayingCard('H', 8);
  }

  @Test
  @DisplayName("Test equals")
  void testEquals() {
    assertFalse(card1.equals(card2));
  }

  @Test
  @DisplayName("Test contructor")
  void testConstructor() {
    assertThrows(IllegalArgumentException.class, () -> {
      card1 = new PlayingCard('X', 12);
    });

    assertThrows(IllegalArgumentException.class, () -> {
      card1 = new PlayingCard('S', 14);
    });

    assertThrows(IllegalArgumentException.class, () -> {
      card1 = new PlayingCard('S', 0);
    });
  }
 }
}
