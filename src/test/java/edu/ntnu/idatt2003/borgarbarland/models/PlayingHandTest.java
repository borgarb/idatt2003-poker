package edu.ntnu.idatt2003.borgarbarland.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

class PlayingHandTest {

  private DeckOfCards deck;
  private PlayingHand hand;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
    hand = new PlayingHand();
  }

  @Test
  void drawCardsFromDeck() {
    hand.drawCardsFromDeck(5, deck);
    assertEquals(5, hand.getHand().size());
    assertEquals(47, deck.getCardStack().size());
  }

  @Test
  void getSumOfFaces() {
    hand = new PlayingHand(){
      @Override
      public List<PlayingCard> getHand() {
        return Arrays.asList(
            new PlayingCard('S', 12),
            new PlayingCard('S', 11),
            new PlayingCard('S', 10),
            new PlayingCard('S', 9),
            new PlayingCard('S', 8));
      }
    };

    int expected = 50;

    assertEquals(expected, hand.getSumOfFaces());
  }

  @Test
  void isFlush() {
    hand = new PlayingHand(){
      @Override
      public List<PlayingCard> getHand() {
        return Arrays.asList(
            new PlayingCard('S', 12),
            new PlayingCard('S', 11),
            new PlayingCard('S', 10),
            new PlayingCard('S', 9),
            new PlayingCard('S', 8)
        );
      }
    };

    assertTrue(hand.isFlush());
  }

  @Test
  void hasQueenOfSpades() {
    hand = new PlayingHand(){
      @Override
      public List<PlayingCard> getHand() {
        return Arrays.asList(
            new PlayingCard('S', 12),
            new PlayingCard('H', 11),
            new PlayingCard('H', 10),
            new PlayingCard('C', 9),
            new PlayingCard('D', 8)
        );
      }
    };

    assertTrue(hand.hasQueenOfSpades());
  }

  @Test
  void getHeartCards() {
    hand = new PlayingHand(){
      @Override
      public List<PlayingCard> getHand() {
        return Arrays.asList(
            new PlayingCard('H', 12),
            new PlayingCard('H', 11),
            new PlayingCard('S', 10),
            new PlayingCard('H', 9),
            new PlayingCard('D', 8)
        );
      }
    };

    assertTrue(hand.getHeartCards().stream().allMatch(card -> card.getSuit() == 'H'));
  }

  @Test
  void getHand() {
    hand = new PlayingHand(){
      @Override
      public List<PlayingCard> getHand() {
        return Arrays.asList(
            new PlayingCard('H', 12),
            new PlayingCard('H', 11),
            new PlayingCard('S', 10),
            new PlayingCard('H', 9),
            new PlayingCard('D', 8)
        );
      }
    };

    assertEquals(5, hand.getHand().size());
  }
}