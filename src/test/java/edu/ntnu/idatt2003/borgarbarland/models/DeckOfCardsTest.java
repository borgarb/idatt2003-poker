package edu.ntnu.idatt2003.borgarbarland.models;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {
  
  private DeckOfCards deck;
  
  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  @DisplayName("Test if the deck adds cards correctly")
  void testAddCard() {
    PlayingCard testCard = new PlayingCard('S', 12);

    deck.addCard(testCard);

    ArrayList<PlayingCard> cardStack = deck.getCardStack();

    assert(cardStack.contains(testCard));
  }

  @Test
  @DisplayName("Test if the deck deals a hand correctly")
  void testDealHand() {
    ArrayList<PlayingCard> currentStack = deck.getCardStack();
    int startStackSize = currentStack.size();

    int dealSize = 5;
    ArrayList<PlayingCard> hand = deck.dealHand(dealSize);

    assert(hand.size() == 5);
    assert(currentStack.size() == startStackSize - dealSize);

    for (PlayingCard card : hand) {
      assert(!currentStack.contains(card));
    }
  }

  @Test
  @DisplayName("Test if the deck gives the correct card stack")
  void testGetCardStack() {
    ArrayList<PlayingCard> cardStack = deck.getCardStack();

    assert(cardStack.size() == 52);
  }

  @Test
  @DisplayName("Test if the deck restocks the card stack correctly")
  void testRestockCardStack() {
    deck.dealHand(14);

    assert(deck.getCardStack().size() == 38);

    deck.restockCardStack();

    assert(deck.getCardStack().size() == 52);
  }
}
